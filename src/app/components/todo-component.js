import React, {Component} from 'react';
import ToDoListItemComponent from './todo-list-item-component';
import ToDoListFooterComponent from './todo-list-footer-component';



export default class ToDoComponent extends React.Component {

  constructor(props) {
    super(props);
    this.toDoModelService = window.toDoModelService;
    this.state = {
      todos: this.toDoModelService.getToDos(),
      nowShowing: this.props.route.nowShowing,
      id: null 
    }
    this.handlerKeyPressEnter = this.handlerKeyPressEnter.bind(this);
    this.toggleAll = this.toggleAll.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
    this.destroy = this.destroy.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.save = this.save.bind(this);
    this.subscription = this.toDoModelService.subscribe(this.onTodosUpdate.bind(this));
    this.editedItem = this.editedItem.bind(this);
    this.clearCompleted = this.clearCompleted.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(this.state.nowShowing !== nextProps.route.nowShowing) {
     if(nextProps.route.nowShowing == "active") {
        this.setState({
            todos: this.toDoModelService.getToDos().filter((item)=> item.completed == false),
            nowShowing: nextProps.route.nowShowing
        });
      }
     if(nextProps.route.nowShowing == "all") {
        this.setState({
            todos: this.toDoModelService.getToDos(),
            nowShowing: nextProps.route.nowShowing
        });
     }
     if(nextProps.route.nowShowing == "completed") {
       this.setState({
            todos: this.toDoModelService.getToDos().filter((item)=> item.completed == true),
            nowShowing: nextProps.route.nowShowing
       });
     } 
    } 
  }
  onTodosUpdate() {
    this.setState({
      todos: this.toDoModelService.getToDos()
    });
  }

  handlerKeyPressEnter(event) {
    if(this.input.value.trim() !== "") {
      var key = event.key;
      if(key == "Enter") {
        this.toDoModelService.addTodo(this.input.value.trim());
        this.input.value = "";
      }
    } 
  }

  destroy(item) {
   this.toDoModelService.destroy(item);
   this.setState({todos: this.state.todos.filter((i)=> i.id !== item.id)});
  }

  subscribe(onChange) {
    this.toDoModelService.subscribe(onChange);
  }

  toggleAll(event) {
    this.toDoModelService.toggleAll(event.target.checked);
  }
  
  clearCompleted() {
       this.toDoModelService.clearCompleted();
  }

  toggleItem(item) {
      this.toDoModelService.toggle(item);
  }

  save(item,title) {
    this.toDoModelService.save(item,title);
  }

  editedItem(idEditedItem) {
    this.setState({id: idEditedItem});
  }

  render() {
    let activeTodoCount = this.state.todos.reduce(function (accum, todo) {
      return todo.completed ? accum : accum + 1;
    }, 0);

    let completedTodoCount = this.toDoModelService.getToDos().reduce(function (accum, todo) {
      return todo.completed ? accum + 1 : accum;
    }, 0);
    return (
      <div className="todo-app">
        <div className="todo-app__header">
          <div className="todo-app__title">React ToDo App</div>
          <input type="checkbox" className="todo-app__toggle-all" checked={activeTodoCount === 0} onChange={this.toggleAll}/>
          <input className="todo-app__new-todo" placeholder="What needs to be done?" onKeyPress={this.handlerKeyPressEnter} ref={(input)=> this.input = input} /></div>
          <div className="todo-app__content">
          <ToDoListItemComponent 
                      todoList={this.state.todos}
                      id={this.state.id} 
                      destroy={this.destroy} 
                      toggleItem={this.toggleItem}
                      save={this.save}
                      editedItem={this.editedItem}
                      nowShowing={this.state.nowShowing} />
          <ToDoListFooterComponent 
                      clearCompleted={this.clearCompleted}
                      nowShowing={this.state.nowShowing}
                      completedTodoCount={completedTodoCount}/>
          </div>
      </div>
    );
  }
}
ToDoComponent.propTypes = {};
