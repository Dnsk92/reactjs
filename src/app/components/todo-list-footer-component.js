import React, {Component} from 'react';
import {Link} from 'react-router';
import classNames from 'classnames/bind';

export default class ToDoListFooterComponent extends Component {

  constructor(props) {
    super(props);
    this.toDoModelService = window.toDoModelService;
  }

  render() {
    return (
          <div className="todo-list__footer">
            <span className="todo-list__info"><strong>{this.toDoModelService.getToDos().filter((item)=> item.completed == false).length}&nbsp;</strong>items left</span>
            <div className="todo-list__controls">
              <div className="tabs">
                <div className={this.props.nowShowing == "all" ? "tabs__item tabs__item--active" : "tabs__item"}><Link to="/">All</Link></div>
                <div className={this.props.nowShowing == "active" ? "tabs__item tabs__item--active" : "tabs__item"}><Link to="/active">Active</Link></div>
                <div className={this.props.nowShowing == "completed" ? "tabs__item tabs__item--active" : "tabs__item"}><Link to="/completed">Completed</Link></div>
              </div>
            </div>
           {this.props.completedTodoCount !== 0 ? <button className="todo-list__clear-completed" onClick={this.props.clearCompleted}>Clear completed</button> : ""} 
          </div>
    );
  }
}
ToDoListFooterComponent.propTypes = {};
