import React, {Component} from 'react';
import TodoItemComponent from './todo-item-component'

export default class ToDoListItemComponent extends Component {

  render() {
    var todoList;
    if(this.props.nowShowing == "completed") {
        todoList = this.props.todoList.filter((item)=> item.completed == true);
     }
     if(this.props.nowShowing == "active") {
        todoList = this.props.todoList.filter((item)=> item.completed == false);
     }
     if(this.props.nowShowing == "all") {
        todoList = this.props.todoList;
     }
    return (
          <div className="todo-list">
              {todoList.map((item)=> <TodoItemComponent 
                                                        key={item.id} 
                                                        item={item}
                                                        id={this.props.id} 
                                                        destroy={this.props.destroy} 
                                                        toggleItem={this.props.toggleItem}
                                                        save={this.props.save}
                                                        editedItem={this.props.editedItem}/>
                                      )
              }
          </div>
    );
  }
}
ToDoListItemComponent.propTypes = {};
