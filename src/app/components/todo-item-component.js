import React, {Component} from 'react';
import classNames from 'classnames/bind';

export default class ToDoItemComponent extends Component {

  constructor(props) {
    super(props);
    this.handlerDoubleClick = this.handlerDoubleClick.bind(this);
    this.handlerEndEdit = this.handlerEndEdit.bind(this);
    this.handlerClickComplete = this.handlerClickComplete.bind(this);
    this.handlerChangeTitle = this.handlerChangeTitle.bind(this);
    this.handlerOnBlur = this.handlerOnBlur.bind(this);
    this.state= {
      title: this.props.item.title
    }
  }

  handlerDoubleClick() {
      this.props.editedItem(this.props.item.id);
  }

  handlerEndEdit(event) {
    var key = event.key;
     if(key == "Enter" && this.state.title !== "") {
        this.props.editedItem(null);
        this.props.save(this.props.item,event.target.value);
     } 
  }

  handlerChangeTitle(event) {
    this.setState({title: event.target.value});
  }

  handlerClickComplete () {
    var checked = this.input.checked;
    this.props.toggleItem(this.props.item);
  }

  handlerOnBlur(event) {
    this.props.save(this.props.item,event.target.value);
    this.setState({title: event.target.value});
    this.props.editedItem(null);
  }

  shouldComponentUpdate(nextProps, nextState) {
      if(this.props.item.id == nextProps.id || nextProps.id == null) {
        return true;
      }  
      return false;
  }

  componentDidUpdate(prevProps, prevState) {
        this.inputFocus.focus();
  }

  render() {
       let classes = classNames({
        "todo-item": true,
        "todo-item todo-item--completed": this.props.item.completed,
        "todo-item todo-item--editing": this.props.item.id == this.props.id,
        });

      return (
            <div className={classes}>
              <div className="todo-item__toggle"><input type="checkbox" value="on" ref={(input)=> this.input = input} onChange={this.handlerClickComplete} checked={this.props.item.completed}/></div>
              <div className="todo-item__text">
                <div className="editable-text">
                  <div className="editable-text__out"><label onDoubleClick={this.handlerDoubleClick}>{this.props.item.title}</label></div>
  <div className="editable-text__input"><input className="edit"  ref={(input)=> this.inputFocus = input}
                                                                value={this.state.title} 
                                                                onKeyPress={this.handlerEndEdit} 
                                                                onChange={this.handlerChangeTitle} 
                                                                onBlur={this.handlerOnBlur}/></div>
                </div>
              </div>
               <div className="todo-item__remover">
                <button className="destroy" onClick={()=> this.props.destroy(this.props.item)} />
              </div>
            </div>
     );  
  }
}
ToDoItemComponent.propTypes = {};